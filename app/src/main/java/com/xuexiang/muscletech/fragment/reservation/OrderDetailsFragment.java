
package com.xuexiang.muscletech.fragment.reservation;

import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;


import com.xuexiang.muscletech.R;
import com.xuexiang.muscletech.core.BaseFragment;
import com.xuexiang.muscletech.databinding.FragmentOrderDetailsBinding;
import com.xuexiang.muscletech.utils.XToastUtils;
import com.xuexiang.xaop.annotation.IOThread;
import com.xuexiang.xaop.annotation.MainThread;
import com.xuexiang.xaop.annotation.SingleClick;
import com.xuexiang.xaop.enums.ThreadType;
import com.xuexiang.xpage.annotation.Page;
import com.xuexiang.xqrcode.XQRCode;
import com.xuexiang.xui.utils.DensityUtils;
import com.xuexiang.xui.widget.dialog.materialdialog.MaterialDialog;
import com.xuexiang.xui.widget.popupwindow.popup.XUIPopup;

import org.json.JSONArray;
import org.json.JSONObject;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


@Page(name = "Order Details")
public class OrderDetailsFragment extends BaseFragment<FragmentOrderDetailsBinding> implements View.OnClickListener  {

    @NonNull
    @Override
    protected FragmentOrderDetailsBinding viewBindingInflate(LayoutInflater inflater, ViewGroup container) {
        return FragmentOrderDetailsBinding.inflate(inflater, container, false);
    }

    private XUIPopup mNormalPopup;

//    /**
//     * @return 返回为 null意为不需要导航栏
//     */
//    @Override
//    protected TitleBar initTitle() {
//        return null;
//    }

    /**
     * 初始化控件
     */
    @Override
    protected void initViews() {
//         get order
        new Thread(new Runnable() {
            @Override
            public void run() {
                try{
                    OkHttpClient client = new OkHttpClient();

                    SharedPreferences sp = getActivity().getSharedPreferences("loginToken", 0);
                    String token = sp.getString("token", null);

                    Request request = new Request.Builder()
                            .url("http://525687l8u9.zicp.vip/order")
                            .header("token", token)
                            .build();

                    Response response = client.newCall(request).execute();
                    String responseData = response.body().string();
                    System.out.println("response data");
                    System.out.println(responseData);
                    JSONObject jsonObject = new JSONObject(responseData);
                    String data = jsonObject.getString("data");
                    JSONArray orderArray = new JSONArray(data);
                    int length = orderArray.length();

                    String []timeslot = {"8:00AM - 10:00AM", "10:00AM - 12:00PM",
                            "14:00PM - 16:00PM", "16:00PM - 18:00PM", "19:00PM - 21:30PM"};
                    String []hour = {"2.00 Hour", "2.00 Hour", "2.00 Hour", "2.00 Hour", "2.50 Hour"};

                    String venueName, dateString, priceString;

                    double [] price = {2, 2, 2, 2, 2.5};



                    if(getActivity() == null){
                        return;
                    }

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            // get time slot
                            TextView M1SLOT = findViewById(R.id.M1SLOT);
                            TextView M2SLOT = findViewById(R.id.M2SLOT);
                            TextView A1SLOT = findViewById(R.id.A1SLOT);
                            TextView A2SLOT = findViewById(R.id.A2SLOT);
                            TextView E1SLOT = findViewById(R.id.E1SLOT);

                            // get price
                            TextView price1 = findViewById(R.id.orderPrice1);
                            TextView price2 = findViewById(R.id.orderPrice2);
                            TextView price3 = findViewById(R.id.orderPrice3);
                            TextView price4 = findViewById(R.id.orderPrice4);
                            TextView price5 = findViewById(R.id.orderPrice5);

                            // get hour
                            TextView m1hour = findViewById(R.id.M1HOUR);
                            TextView m2hour = findViewById(R.id.M2HOUR);
                            TextView a1hour = findViewById(R.id.A1HOUR);
                            TextView a2hour = findViewById(R.id.A2HOUR);
                            TextView e1hour = findViewById(R.id.E1HOUR);

                            // get date
                            TextView m1date = findViewById(R.id.m1date);
                            TextView m2date = findViewById(R.id.m2date);
                            TextView a1date = findViewById(R.id.a1date);
                            TextView a2date = findViewById(R.id.a2date);
                            TextView e1date = findViewById(R.id.e1date);

                            // get venue
                            TextView m1venue = findViewById(R.id.m1venue);
                            TextView m2venue = findViewById(R.id.m2venue);
                            TextView a1venue = findViewById(R.id.a1venue);
                            TextView a2venue = findViewById(R.id.a2venue);
                            TextView e1venue = findViewById(R.id.e1venue);

                            // eliminate cardView
                            eliminateCard(length);


                            JSONObject orderInfo;
                            int orderTime;

                            try{


                            // first order
                            if(length >=1 ){
                                orderInfo = orderArray.getJSONObject(0);
                                orderTime = orderInfo.getInt("orderTime") - 1;
                                M1SLOT.setText(timeslot[orderTime]);
                                m1hour.setText(hour[orderTime]);
                                price1.setText(String.valueOf(orderInfo.getInt("price") * price[orderTime]));
                                m1venue.setText(orderInfo.getString("venueName"));
                                m1date.setText(orderInfo.getString("orderDate"));
                            }

                            if(length >= 2){
                                orderInfo = orderArray.getJSONObject(1);
                                orderTime = orderInfo.getInt("orderTime") - 1;
                                M2SLOT.setText(timeslot[orderTime]);
                                m2hour.setText(hour[orderTime]);
                                price2.setText(String.valueOf(orderInfo.getInt("price") * price[orderTime]));
                                m2venue.setText(orderInfo.getString("venueName"));
                                m2date.setText(orderInfo.getString("orderDate"));
                            }

                            if(length >= 3){
                                orderInfo = orderArray.getJSONObject(2);
                                orderTime = orderInfo.getInt("orderTime") - 1;
                                A1SLOT.setText(timeslot[orderTime]);
                                a1hour.setText(hour[orderTime]);
                                price3.setText(String.valueOf(orderInfo.getInt("price") * price[orderTime]));
                                a1venue.setText(orderInfo.getString("venueName"));
                                a1date.setText(orderInfo.getString("orderDate"));
                            }

                            if (length >= 4){
                                orderInfo = orderArray.getJSONObject(3);
                                orderTime = orderInfo.getInt("orderTime") - 1;
                                A2SLOT.setText(timeslot[orderTime]);
                                a2hour.setText(hour[orderTime]);
                                price4.setText(String.valueOf(orderInfo.getInt("price") * price[orderTime]));
                                a2venue.setText(orderInfo.getString("venueName"));
                                a2date.setText(orderInfo.getString("orderDate"));
                            }

                            if (length >= 5){
                                orderInfo = orderArray.getJSONObject(4);
                                orderTime = orderInfo.getInt("orderTime") - 1;
                                E1SLOT.setText(timeslot[orderTime]);
                                e1hour.setText(hour[orderTime]);
                                price5.setText(String.valueOf(orderInfo.getInt("price") * price[orderTime]));
                                e1venue.setText(orderInfo.getString("venueName"));
                                e1date.setText(orderInfo.getString("orderDate"));
                            }
                            }catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private void eliminateCard(int length){
        // get card view
        CardView M1 = findViewById(R.id.M11);
        CardView M2 = findViewById(R.id.M22);
        CardView A1 = findViewById(R.id.A11);
        CardView A2 = findViewById(R.id.A22);
        CardView E1 = findViewById(R.id.E11);
        if(length <= 4){
            E1.setVisibility(View.INVISIBLE);
        }
        if(length <= 3){
            A2.setVisibility(View.INVISIBLE);
        }
        if(length <= 2){
            A1.setVisibility(View.INVISIBLE);
        }
        if(length <= 1){
            M2.setVisibility(View.INVISIBLE);
        }
        if(length <= 0){
            M1.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    protected void initListeners() {
        binding.M11.setOnClickListener(this);
        binding.M22.setOnClickListener(this);
        binding.A11.setOnClickListener(this);
        binding.A22.setOnClickListener(this);
        binding.E11.setOnClickListener(this);
    }

    @SingleClick
    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.M11) {
            createQRCodeWithLogo(v);
        } else if (id == R.id.M22) {
            createQRCodeWithLogo(v);
        }else if (id == R.id.A11) {
            createQRCodeWithLogo(v);
        }else if (id == R.id.A22) {
            createQRCodeWithLogo(v);
        }else if (id == R.id.E11) {
            createQRCodeWithLogo(v);
        }
    }



    private void createQRCodeWithLogo(View v) {
        showQRCode(XQRCode.createQRCodeWithLogo("Use this QR code to enter sport centre", 400, 400, null));
        mNormalPopup.setAnimStyle(XUIPopup.ANIM_GROW_FROM_CENTER);
        mNormalPopup.setPreferredDirection(XUIPopup.DIRECTION_TOP);
        mNormalPopup.show(v);
    }

    private void showQRCode(Bitmap QRCode) {
        if (mNormalPopup == null) {
            mNormalPopup = new XUIPopup(getContext());
            ImageView imageView = new ImageView(getContext());
            imageView.setLayoutParams(mNormalPopup.generateLayoutParam(
                    DensityUtils.dp2px(getContext(), 250),
                    WRAP_CONTENT
            ));
            imageView.setImageBitmap(QRCode);
            mNormalPopup.setContentView(imageView);
        }

    }

}
