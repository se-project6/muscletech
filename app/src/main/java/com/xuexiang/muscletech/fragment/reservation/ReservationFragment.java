/*
 * Copyright (C) 2019 xuexiangjys(xuexiangjys@163.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.xuexiang.muscletech.fragment.reservation;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.xuexiang.muscletech.R;
import com.xuexiang.muscletech.core.BaseFragment;
import com.xuexiang.muscletech.databinding.FragmentReservationBinding;
import com.xuexiang.muscletech.fragment.reservation.ReservationSearchFragment;
import com.xuexiang.xaop.annotation.SingleClick;
import com.xuexiang.xpage.annotation.Page;
import com.xuexiang.xpage.enums.CoreAnim;
import com.xuexiang.xui.widget.actionbar.TitleBar;


@Page(anim = CoreAnim.none)
public class ReservationFragment extends BaseFragment<FragmentReservationBinding> implements View.OnClickListener {

    private static final String KEY_VENUE_NAME = "venue_name";

    @NonNull
    @Override
    protected FragmentReservationBinding viewBindingInflate(LayoutInflater inflater, ViewGroup container) {
        return FragmentReservationBinding.inflate(inflater, container, false);
    }



    /**
     * 初始化控件
     */
    @Override
    protected void initViews() {

    }

    @Override
    protected void initListeners() {
        binding.reserve.setOnClickListener(this);
        binding.reserve2.setOnClickListener(this);
        binding.reserve3.setOnClickListener(this);
        binding.reserve4.setOnClickListener(this);
        binding.reserve5.setOnClickListener(this);
        binding.reserve6.setOnClickListener(this);
    }

    @SingleClick
    @Override
    public void onClick(View v) {
        int id = v.getId();
        if(id == R.id.reserve) {
            openNewPage(ReservationSearchFragment.class, KEY_VENUE_NAME, "Swimming");
        } else if(id == R.id.reserve2) {
            openNewPage(ReservationSearchFragment.class, KEY_VENUE_NAME, "Ping Pong");
        } else if(id == R.id.reserve3) {
            openNewPage(ReservationSearchFragment.class, KEY_VENUE_NAME, "Badminton");
        } else if(id == R.id.reserve4) {
            openNewPage(ReservationSearchFragment.class, KEY_VENUE_NAME, "Gym");
        } else if (id == R.id.reserve5) {
            openNewPage(ReservationSearchFragment.class, KEY_VENUE_NAME, "Tennis");
        } else if(id == R.id.reserve6) {
            openNewPage(ReservationSearchFragment.class, KEY_VENUE_NAME, "Golf");
        }
    }


}
