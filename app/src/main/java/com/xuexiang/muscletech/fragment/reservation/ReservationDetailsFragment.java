/*
 * Copyright (C) 2019 xuexiangjys(xuexiangjys@163.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.xuexiang.muscletech.fragment.reservation;

import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import com.xuexiang.muscletech.R;
import com.xuexiang.muscletech.core.BaseFragment;
import com.xuexiang.muscletech.databinding.FragmentReservationDetailsBinding;
import com.xuexiang.xaop.annotation.SingleClick;
import com.xuexiang.xpage.annotation.Page;

@Page(name = "Reservation Details")
public class ReservationDetailsFragment extends BaseFragment<FragmentReservationDetailsBinding> implements View.OnClickListener  {
    private static final String KEY_PRICE = "price";
    private int mprice;

    @NonNull
    @Override
    protected FragmentReservationDetailsBinding viewBindingInflate(LayoutInflater inflater, ViewGroup container) {
        return FragmentReservationDetailsBinding.inflate(inflater, container, false);
    }

//    /**
//     * @return 返回为 null意为不需要导航栏
//     */
//    @Override
//    protected TitleBar initTitle() {
//        return null;
//    }

    /**
     * 初始化控件
     */
    @Override
    protected void initViews() {
        // get the coach and prices
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("venueInfo", 0);
        SharedPreferences sp1 = getActivity().getSharedPreferences("searchFiled", 0);

        String coach = sharedPreferences.getString("coach", null);
        int price = sharedPreferences.getInt("price", 0);
        String sportsCenterName = sharedPreferences.getString("sportsCenterName", null);
        String venueName = sharedPreferences.getString("venueName", null);
        String date = sharedPreferences.getString("date", null);

        SharedPreferences sp = getActivity().getSharedPreferences("searchField", 0);
        System.out.println("search field");
        System.out.println(sp.getString("venueName", null));
        System.out.println(sp.getString("sportsCenterName", null));
        System.out.println(sp.getString("date", null));

//        System.out.println(sportsCenterName);
//        System.out.println(venueName);
//        System.out.println("price");
//        System.out.println(price);
        mprice = price;

        // set coach
        TextView tv = findViewById(R.id.coach1);
        tv.setText(coach);
        tv = findViewById(R.id.coach2);
        tv.setText(coach);
        tv = findViewById(R.id.coach3);
        tv.setText(coach);
        tv = findViewById(R.id.coach4);
        tv.setText(coach);
        tv = findViewById(R.id.coach5);
        tv.setText(coach);

        // set price
        tv = findViewById(R.id.price1);
        tv.setText(String.valueOf(2*price));
        tv = findViewById(R.id.price2);
        tv.setText(String.valueOf(2*price));
        tv = findViewById(R.id.price3);
        tv.setText(String.valueOf(2*price));
        tv = findViewById(R.id.price4);
        tv.setText(String.valueOf(2*price));
        tv = findViewById(R.id.price5);
        tv.setText(String.valueOf(2.5 * price));

        // set sports center name
        tv = findViewById(R.id.sportsCenterName);
        tv.setText(sp.getString("sportsCenterName", null));

        tv = findViewById(R.id.venueName);
        tv.setText(sp.getString("venueName", null));

        tv = findViewById(R.id.date);
        tv.setText(sp.getString("date", null));
    }


    @Override
    protected void initListeners() {
        binding.M1.setOnClickListener(this);
        binding.M2.setOnClickListener(this);
        binding.A1.setOnClickListener(this);
        binding.A2.setOnClickListener(this);
        binding.E1.setOnClickListener(this);
    }

    @SingleClick
    @Override
    public void onClick(View v) {
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("venueInfo", 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        int id = v.getId();
        if (id == R.id.M1) {
            editor.putInt("price", 2*mprice);
            editor.putInt("orderTime", 1);
            editor.commit();
            openNewPage(PaymentDetailsFragment.class, KEY_PRICE, 2 * mprice);
        } else if (id == R.id.M2) {
            editor.putInt("price", 2*mprice);
            editor.putInt("orderTime", 2);
            editor.commit();
            openNewPage(PaymentDetailsFragment.class, KEY_PRICE, 2 * mprice);
        }else if (id == R.id.A1) {
            editor.putInt("price", 2*mprice);
            editor.putInt("orderTime", 3);
            editor.commit();
            openNewPage(PaymentDetailsFragment.class, KEY_PRICE, 2 * mprice);
        }else if (id == R.id.A2) {
            editor.putInt("price", 2*mprice);
            editor.putInt("orderTime", 4);
            editor.commit();
            openNewPage(PaymentDetailsFragment.class, KEY_PRICE, 2 * mprice);
        }else if (id == R.id.E1) {
            editor.putInt("price", (int) (2.5*mprice));
            editor.putInt("orderTime", 5);
            editor.commit();
            openNewPage(PaymentDetailsFragment.class, KEY_PRICE, 2.5 * mprice);
        }
    }

}
