/*
 * Copyright (C) 2019 xuexiangjys(xuexiangjys@163.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.xuexiang.muscletech.fragment.payment;

import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

import com.xuexiang.muscletech.core.BaseFragment;
import com.xuexiang.muscletech.databinding.FragmentPaymentBinding;

import com.xuexiang.muscletech.utils.XToastUtils;
import com.xuexiang.xaop.annotation.SingleClick;
import com.xuexiang.xpage.annotation.Page;
import com.xuexiang.muscletech.R;
import com.xuexiang.xui.XUI;
import com.xuexiang.xui.adapter.simple.XUISimpleAdapter;
import com.xuexiang.xui.utils.DensityUtils;
import com.xuexiang.xui.widget.popupwindow.popup.XUIListPopup;
import com.xuexiang.xui.widget.popupwindow.popup.XUIPopup;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


@Page(name = "Payment")
public class PaymentFragment extends BaseFragment<FragmentPaymentBinding> implements View.OnClickListener {
    private XUIListPopup mListPopup;
    private int mcardNumber;
    @NonNull
    @Override
    protected FragmentPaymentBinding viewBindingInflate(LayoutInflater inflater, ViewGroup container) {
        return FragmentPaymentBinding.inflate(inflater, container, false);
    }

    /**
     * 初始化控件
     */
    @Override
    protected void initViews() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                SharedPreferences sp = getActivity().getSharedPreferences("loginToken", 0);
                String token = sp.getString("token", null);
                Request request = new Request.Builder()
                        .url("http://525687l8u9.zicp.vip/card")
                        .header("token", token)
                        .build();
                OkHttpClient client = new OkHttpClient();
                Response response = null;
                    response = client.newCall(request).execute();
                    String responseData = null;
                    responseData = response.body().string();
                    System.out.println("response data");
                    System.out.println(responseData);
                    JSONObject jsonObject = new JSONObject(responseData);
                    String data = jsonObject.getString("data");
                    JSONArray jsonArray = new JSONArray(data);
                    TextView cardNumber = findViewById(R.id.cardNumber);
                    TextView holderName = findViewById(R.id.holderName);
                    System.out.println("length");
                    System.out.println(jsonArray.length());
                    mcardNumber = jsonArray.length();
                    if(jsonArray.length() == 0){
                        if(getActivity() == null)
                            return;
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                cardNumber.setText("No card");
                            }
                        });

                    }else{
                        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("cardInfo", 0);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putInt("cardId", jsonArray.getJSONObject(0).getInt("cardId"));
                        editor.commit();
                        if(getActivity() == null)
                            return;
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try{
                                    holderName.setText(jsonArray.getJSONObject(0).getString("cardName"));
                                    cardNumber.setText(jsonArray.getJSONObject(0).getString("cardNumber"));

                                }catch (JSONException e){
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }).start();
    }

    @Override
    protected void initListeners() {
        binding.addCard.setOnClickListener(this);
        binding.options.setOnClickListener(this);
    }

    @SingleClick
    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.add_card) {
            if(mcardNumber == 0){
                openPage(CardAdditionFragment.class,false);
            }else{
                XToastUtils.toast("Card Exists");
            }
        } else if (id == R.id.options) {
            initListPopupIfNeed();
            mListPopup.setAnimStyle(XUIPopup.ANIM_GROW_FROM_CENTER);
            mListPopup.setPreferredDirection(XUIPopup.DIRECTION_BOTTOM);
            mListPopup.show(v);
        }
    }


    private void initListPopupIfNeed() {
        if (mListPopup == null) {

            String[] listItems = new String[]{
                    "Delete",
            };

            XUISimpleAdapter adapter = XUISimpleAdapter.create(getContext(), listItems);
            mListPopup = new XUIListPopup(getContext(), adapter);
            mListPopup.create(DensityUtils.dp2px(200), DensityUtils.dp2px(150), (adapterView, view, i, l) -> {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        SharedPreferences sp = getActivity().getSharedPreferences("loginToken", 0);
                        String token = sp.getString("token", null);
                        SharedPreferences sp1 = getActivity().getSharedPreferences("cardInfo", 0);
                        int cardId = sp1.getInt("cardId", 0);
                        String url = "http://525687l8u9.zicp.vip/card/" + String.valueOf(cardId);
                        OkHttpClient client = new OkHttpClient();
                        Request request = new Request.Builder()
                                .url(url)
                                .delete()
                                .build();
                        try{
                            Response response = client.newCall(request).execute();
                            String responseData = response.body().string();
                            System.out.println(responseData);
                            try{
                                JSONObject jsonObject = new JSONObject(responseData);
                                String code = jsonObject.getString("code");
                                if(code.equals("0")){
                                    if(getActivity() == null){
                                        return;
                                    }
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            XToastUtils.toast("Card Deleted");
                                            mListPopup.dismiss();
                                            popToBack();
                                        }
                                    });
                                }
                            }catch (JSONException e){
                                e.printStackTrace();
                            }
                        }catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }).start();
            });
        }
    }
}
