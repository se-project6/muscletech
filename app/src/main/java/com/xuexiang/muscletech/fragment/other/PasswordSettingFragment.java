/*
 * Copyright (C) 2019 xuexiangjys(xuexiangjys@163.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.xuexiang.muscletech.fragment.other;

import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.xuexiang.muscletech.R;
import com.xuexiang.muscletech.core.BaseFragment;
import com.xuexiang.muscletech.databinding.FragmentPasswordSettingBinding;
import com.xuexiang.muscletech.databinding.FragmentPaymentDetailsBinding;
import com.xuexiang.muscletech.utils.XToastUtils;
import com.xuexiang.xaop.annotation.SingleClick;
import com.xuexiang.xpage.annotation.Page;
import com.xuexiang.xrouter.annotation.AutoWired;
import com.xuexiang.xrouter.launcher.XRouter;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

@Page(name = "Password setting")
public class PasswordSettingFragment extends BaseFragment<FragmentPasswordSettingBinding> implements View.OnClickListener {

    @NonNull
    @Override
    protected FragmentPasswordSettingBinding viewBindingInflate(LayoutInflater inflater, ViewGroup container) {
        return FragmentPasswordSettingBinding.inflate(inflater, container, false);
    }
    /**
     * 初始化控件
     */
    @Override
    protected void initViews() {

    }

    @Override
    protected void initListeners() {
        binding.passwordSetting.setOnClickListener(this);
    }

    @SingleClick
    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.password_setting) {
            String password1 = binding.password1.getEditValue();
            String password2 = binding.password2.getEditValue();
            if (password1.equals("") | password2.equals("")){
                XToastUtils.toast("Please enter password!");
            }else if(!password1.equals(password2)){
                XToastUtils.toast("Two different password!");
            }else{
                setPassword(password1, password2);
            }
        }
    }

    public void setPassword(String password1, String password2){

        new Thread(new Runnable() {
            @Override
            public void run() {
                try{
                    String json = "{\n" +
                            "\"newPassword\":" + "\"" + password1 + "\"" +
                            ",\n\"password\":" + "\"" + password2 + "\"" +
                            "}";

                    SharedPreferences sp = getActivity().getSharedPreferences("loginToken", 0);
                    String token = sp.getString("token", null);


                    System.out.println(json);
                    OkHttpClient client = new OkHttpClient();
                    Request request = new Request.Builder()
                            .url("http://525687l8u9.zicp.vip/user/password")
                            .header("token", token)
                            .post(RequestBody.create(MediaType.parse("application/json"), json))
                            .build();

                    Response response = client.newCall(request).execute();
                    String responseData = response.body().string();
                    System.out.println("responseData");
                    System.out.println(responseData);
                    if(getActivity() == null)
                        return;
                } catch (IOException e) {
                    e.printStackTrace();
                    System.out.println("This is exception");
                    if(getActivity() == null)
                        return;
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            XToastUtils.toast("Connection Failed!");
                        }
                    });
                }
            }
        }).start();
        popToBack();
    }


    @Override
    protected void initArgs() {
        XRouter.getInstance().inject(this);
    }
}
