/*
 * Copyright (C) 2021 xuexiangjys(xuexiangjys@163.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.xuexiang.muscletech.fragment.other;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.xuexiang.muscletech.R;
import com.xuexiang.muscletech.activity.MainActivity;
import com.xuexiang.muscletech.core.BaseFragment;
import com.xuexiang.muscletech.databinding.FragmentLoginVerifyCodeBinding;
import com.xuexiang.muscletech.fragment.news.NewsFragment;
import com.xuexiang.muscletech.fragment.reservation.ReservationFragment;
import com.xuexiang.muscletech.utils.RandomUtils;
import com.xuexiang.muscletech.utils.SettingUtils;
import com.xuexiang.muscletech.utils.TokenUtils;
import com.xuexiang.muscletech.utils.Utils;
import com.xuexiang.muscletech.utils.XToastUtils;
import com.xuexiang.muscletech.utils.sdkinit.UMengInit;
import com.xuexiang.xaop.annotation.SingleClick;
import com.xuexiang.xpage.annotation.Page;
import com.xuexiang.xpage.enums.CoreAnim;
import com.xuexiang.xui.utils.CountDownButtonHelper;
import com.xuexiang.xui.utils.ResUtils;
import com.xuexiang.xui.utils.ThemeUtils;
import com.xuexiang.xui.utils.ViewUtils;
import com.xuexiang.xui.widget.actionbar.TitleBar;
import com.xuexiang.xutil.app.ActivityUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


/**
 * 注册页面
 *
 */
@Page(anim = CoreAnim.none)
public class LoginVerifyCodeFragment extends BaseFragment<FragmentLoginVerifyCodeBinding> implements View.OnClickListener {

    private View mJumpView;

    private CountDownButtonHelper mCountDownHelper;

    @NonNull
    @Override
    protected FragmentLoginVerifyCodeBinding viewBindingInflate(LayoutInflater inflater, ViewGroup container) {
        return FragmentLoginVerifyCodeBinding.inflate(inflater, container, false);
    }

    @Override
    protected TitleBar initTitle() {
        TitleBar titleBar = super.initTitle()
                .setImmersive(true);
        titleBar.setBackgroundColor(Color.TRANSPARENT);
        titleBar.setTitle("");
        titleBar.setLeftImageDrawable(ResUtils.getVectorDrawable(getContext(), R.drawable.ic_login_close));
        titleBar.setActionTextColor(ThemeUtils.resolveColor(getContext(), R.attr.colorAccent));
//        mJumpView = titleBar.addAction(new TitleBar.TextAction(R.string.title_jump_login) {
//            @Override
//            public void performAction(View view) {
//                onLoginVerifyCodeSuccess();
//            }
//        });
        return titleBar;
    }

    @Override
    protected void initViews() {
        mCountDownHelper = new CountDownButtonHelper(binding.btnGetVerifyCode, 60);
        //隐私政策弹窗
        if (!SettingUtils.isAgreePrivacy()) {
            Utils.showPrivacyDialog(getContext(), (dialog, which) -> {
                dialog.dismiss();
                handleSubmitPrivacy();
            });
        }
        boolean isAgreePrivacy = SettingUtils.isAgreePrivacy();
        binding.cbProtocol.setChecked(isAgreePrivacy);
        refreshButton(isAgreePrivacy);
        binding.cbProtocol.setOnCheckedChangeListener((buttonView, isChecked) -> {
            SettingUtils.setIsAgreePrivacy(isChecked);
            refreshButton(isChecked);
        });
    }

    @Override
    protected void initListeners() {
        binding.btnGetVerifyCode.setOnClickListener(this);
        binding.btnLoginVerifyCode.setOnClickListener(this);
        binding.tvLoginPassword.setOnClickListener(this);
        binding.tvUserProtocol.setOnClickListener(this);
        binding.tvPrivacyProtocol.setOnClickListener(this);
    }

    private void refreshButton(boolean isChecked) {
        ViewUtils.setEnabled(binding.btnLoginVerifyCode, isChecked);
        ViewUtils.setEnabled(mJumpView, isChecked);
    }

    private void handleSubmitPrivacy() {
        SettingUtils.setIsAgreePrivacy(true);
        UMengInit.init();
        // 应用市场不让默认勾选
//        ViewUtils.setChecked(cbProtocol, true);
    }

    @SingleClick
    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.btn_get_verify_code) {
            if (binding.etLoginVerifyCodePhoneNumber.validate()) {
                getVerifyCode(binding.etLoginVerifyCodePhoneNumber.getEditValue());
            }
        } else if (id == R.id.btn_login_verify_code) {
            if (binding.etLoginVerifyCodePhoneNumber.validate()) {
                if (binding.etVerifyCode.validate()) {
                    loginByPassWordVerifyCode(binding.etLoginVerifyCodePhoneNumber.getEditValue(), binding.etLoginVerifyCodePhoneNumber.getEditValue(), binding.etVerifyCode.getEditValue());
                }
            }
        } else if (id == R.id.tv_login_password) {
            openNewPage(LoginPasswordFragment.class);
        } else if (id == R.id.tv_user_protocol) {
            Utils.gotoProtocol(this, false, true);
        } else if (id == R.id.tv_privacy_protocol) {
            Utils.gotoProtocol(this, true, true);
        }

    }

    /**
     * 获取验证码
     */
    private void getVerifyCode(String phoneNumber) {

        XToastUtils.warning("Only Test!");
        mCountDownHelper.start();
    }

    /**
     * 根据验证码注册
     *
     * @param phoneNumber 手机号
     * @param password 手机号
     * @param verifyCode  验证码
     */
    private void loginByPassWordVerifyCode(String phoneNumber, String password ,String verifyCode) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try{
                    String json = "{\n" +
                            "\n\"userPhoneNumber\":" + "\"" + phoneNumber + "\"" +
                            "}";

                    System.out.println(json);
                    OkHttpClient client = new OkHttpClient();
                    Request request = new Request.Builder()
                            .url("http://525687l8u9.zicp.vip/user/login/verification")
                            .post(RequestBody.create(MediaType.parse("application/json"), json))
                            .build();

                    Response response = client.newCall(request).execute();
                    String responseData = response.body().string();
                    System.out.println("responseData");
                    System.out.println(responseData);
                    try{
                        JSONObject jsonObject=new JSONObject(responseData);
                        String data = jsonObject.getString("data");
                        JSONObject userInfo = new JSONObject(data);
                        String token = userInfo.getString("token");
                        System.out.println("token");
                        System.out.println(token);
//                        SharedPreferences sp = getContext().getSharedPreferences("loginToken", 0);
                        SharedPreferences sp = getActivity().getSharedPreferences("loginToken", 0);
                        SharedPreferences.Editor editor = sp.edit();
                        editor.putString("token", token);
                        editor.commit();
                        if(getActivity() == null){
                            return;
                        }
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                XToastUtils.success("Logged in!");
                            }
                        });
                        ActivityUtils.startActivity(MainActivity.class);
                    }catch (JSONException e){
                        e.printStackTrace();
                    }

                    if(getActivity() == null)
                        return;
                } catch (IOException e) {
                    e.printStackTrace();
                    System.out.println("This is exception");
                    if(getActivity() == null)
                        return;
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            XToastUtils.toast("Connection Failed!");
                        }
                    });
                }
            }
        }).start();
//        onLoginVerifyCodeSuccess();
    }

    /**
     * 注册成功的处理
     */
    private void onLoginVerifyCodeSuccess() {
        String token = RandomUtils.getRandomNumbersAndLetters(16);
        if (TokenUtils.handleLoginSuccess(token)) {
            popToBack();
            ActivityUtils.startActivity(MainActivity.class);
        }
    }

    @Override
    public void onDestroyView() {
        if (mCountDownHelper != null) {
            mCountDownHelper.recycle();
        }
        super.onDestroyView();
    }



}

