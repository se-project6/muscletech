/*
 * Copyright (C) 2019 xuexiangjys(xuexiangjys@163.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.xuexiang.muscletech.fragment.reservation;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import com.xuexiang.muscletech.R;
import com.xuexiang.muscletech.activity.MainActivity;
import com.xuexiang.muscletech.core.BaseFragment;
import com.xuexiang.muscletech.databinding.FragmentReservationBinding;
import com.xuexiang.muscletech.databinding.FragmentReservationSearchBinding;
import com.xuexiang.muscletech.fragment.reservation.ReservationDetailsFragment;
import com.xuexiang.muscletech.utils.XToastUtils;
import com.xuexiang.xaop.annotation.SingleClick;
import com.xuexiang.xpage.annotation.Page;
import com.xuexiang.xpage.enums.CoreAnim;
import com.xuexiang.xrouter.annotation.AutoWired;
import com.xuexiang.xrouter.launcher.XRouter;
import com.xuexiang.xui.utils.ResUtils;
import com.xuexiang.xui.widget.actionbar.TitleBar;
import com.xuexiang.xui.widget.picker.widget.OptionsPickerView;
import com.xuexiang.xui.widget.picker.widget.builder.OptionsPickerBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Calendar;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


@Page(name = "Search", params = {ReservationSearchFragment.KEY_VENUE_NAME})
public class ReservationSearchFragment extends BaseFragment<FragmentReservationSearchBinding> implements View.OnClickListener {

//    /**
//     * @return 返回为 null意为不需要导航栏
//     */
//    @Override
//    protected TitleBar initTitle() {
//        return null;
//    }

    /**
     * advanced form achievement
     */
    private TextView datePicker;
    private TextView locationPicker;
    private String[] mLocationOption;
    private int locationSelectOption = 0;
    private String dateString;

    /**
     * Search title set
     */
    public final static String KEY_VENUE_NAME = "venue_name";
    @AutoWired(name = KEY_VENUE_NAME)
    String venueName;

    /**
     * basic init
     */
    @NonNull
    @Override
    protected FragmentReservationSearchBinding viewBindingInflate(LayoutInflater inflater, ViewGroup container) {
        return FragmentReservationSearchBinding.inflate(inflater, container, false);
    }

    @Override
    protected void initViews() {
        update();

        datePicker = findViewById(R.id.date_picker);
        datePicker.setText(getTodaysDate());

        locationPicker = findViewById(R.id.location_picker);
        locationSelectOption = 0;
        mLocationOption = ResUtils.getStringArray(R.array.location_option);
        locationPicker.setText(mLocationOption[locationSelectOption]);
//        locationPicker.setText("Sports Center Name");


    }

    /**
     * click listener init
     */

    @Override
    protected void initListeners() {
        binding.locationPicker.setOnClickListener(this);
        binding.datePicker.setOnClickListener(this);
        binding.searchField.setOnClickListener(this);
    }

    @SingleClick
    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.searchField) {
            String sportsCenterName = mLocationOption[locationSelectOption];
            dateString = ((TextView) datePicker).getText().toString();
            searchFiled(venueName, sportsCenterName, dateString);
        } else if (id == R.id.date_picker){
            showDatePicker(getContext(), DatePickerDialog.THEME_DEVICE_DEFAULT_LIGHT, (TextView) datePicker, Calendar.getInstance());
        } else if (id == R.id.location_picker){
            showLocationPickerView();
        }
    }

    private void searchFiled(String venueName,
                             String sportsCenterName, String date){
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("searchField", 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("venueName", venueName);
        editor.putString("sportsCenterName", sportsCenterName);
        editor.putString("date", date);
        editor.commit();

        SharedPreferences sp = getActivity().getSharedPreferences("searchField", 0);
        System.out.println(sp.getString("venueName", null));
        System.out.println(sp.getString("sportsCenterName", null));
        System.out.println(sp.getString("date", null));

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    System.out.println("This is HTTP request");
                    String json = "{\n" +
                            "\"centerName\":" + "\"" + sportsCenterName + "\"" +
                            ",\n\"venueName\":" + "\"" + venueName + "\"" +
                            "}";
                    System.out.println("json");
//                    System.out.println(json);
                    OkHttpClient client = new OkHttpClient();

                    Request request = new Request.Builder()
                            .url("http://525687l8u9.zicp.vip/venue/app")
                            .post(RequestBody.create(MediaType.parse("application/json"), json))
                            .build();

//                    System.out.println("response data1");

                    Response response = client.newCall(request).execute();
//                    if (!response.isSuccessful())
//                        System.err.println("");
                    String responseData = response.body().string();
//                    System.out.println("response data");
//                    System.out.println(responseData);
                    try {
                        JSONObject jsonObject = new JSONObject(responseData);
                        String data = jsonObject.getString("data");
                        JSONArray venueInfoArray = new JSONArray(data);
                        if(venueInfoArray.length() == 0){
                            if(getActivity() == null){
                                return;
                            }
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    XToastUtils.toast("No available venue!");

                                }
                            });
                        }else{
                            JSONObject venueInfo = venueInfoArray.getJSONObject(0);
                            System.out.println(venueInfo);
                            SharedPreferences sp1 = getActivity().getSharedPreferences("searchField", 0);
                            SharedPreferences sp = getActivity().getSharedPreferences("venueInfo", 0);
                            SharedPreferences.Editor editor1 = sp.edit();
                            editor1.putInt("venueId", venueInfo.getInt("venueId"));
                            editor1.putString("coach", venueInfo.getString("coach"));
                            editor1.putInt("price", venueInfo.getInt("price"));
                            editor1.putString("date", sp1.getString("date", null));
                            editor1.commit();

                            SharedPreferences sharedPreferences = getActivity().getSharedPreferences("venueInfo", 0);
                            String coach = sharedPreferences.getString("coach", null);
                            int price = sharedPreferences.getInt("price", 0);
                            openNewPage(ReservationDetailsFragment.class);
                        }
                    }catch (JSONException e){
                        e.printStackTrace();
                    }
                    if(getActivity() == null)
                        return;
                } catch (IOException e) {
                    e.printStackTrace();
                    if(getActivity() == null)
                        return;
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            XToastUtils.toast("Connection Failed");
                        }
                    });
                }
            }
        }).start();

    }


    /**
     * location choice
     */
    private void showLocationPickerView() {
        OptionsPickerView pvOptions = new OptionsPickerBuilder(getContext(), (v, options1, options2, options3) -> {
            locationPicker.setText(mLocationOption[options1]);
            locationSelectOption = options1;
            return false;
        })
                .setTitleText("Location Selection")
                .setSelectOptions(locationSelectOption)
                .build();
        pvOptions.setPicker(mLocationOption);
        pvOptions.show();
    }

    /**
     * date choice
     * @param context
     * @param themeResId
     * @param tv
     * @param calendar
     */

    public void showDatePicker(Context context, int themeResId, final TextView tv, Calendar calendar) {
        new DatePickerDialog(context
                , themeResId
                , (view, year, monthOfYear, dayOfMonth) -> tv.setText(makeDateString(dayOfMonth, monthOfYear+1,year))
                // 设置初始日期
                , calendar.get(Calendar.YEAR)
                , calendar.get(Calendar.MONTH)
                , calendar.get(Calendar.DAY_OF_MONTH))
                .show();
    }

    private String getTodaysDate() {
        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        month = month + 1;
        int day = cal.get(Calendar.DAY_OF_MONTH);
        return makeDateString(day, month, year);
    }

    private String makeDateString(int day, int month, int year) {
        return month + "-" + day + "-" + year;
    }

    /**
     * search title update
     */
    private void update() {
        binding.searchTitle.setText(String.format("Welcome to %s", venueName));
    }

    @Override
    protected void initArgs() {
        XRouter.getInstance().inject(this);
    }

//    boolean isChanged;
//    @Override
//    public void onSaveInstanceState(@NonNull Bundle outState) {
//        super.onSaveInstanceState(outState);
//        isChanged = true;
//    }
}
