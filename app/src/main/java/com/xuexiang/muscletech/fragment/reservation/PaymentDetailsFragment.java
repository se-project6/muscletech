/*
 * Copyright (C) 2019 xuexiangjys(xuexiangjys@163.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.xuexiang.muscletech.fragment.reservation;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import com.xuexiang.muscletech.R;
import com.xuexiang.muscletech.core.BaseFragment;
import com.xuexiang.muscletech.databinding.FragmentPaymentDetailsBinding;
import com.xuexiang.muscletech.fragment.other.LoginPasswordFragment;
import com.xuexiang.muscletech.fragment.payment.CardAdditionFragment;
import com.xuexiang.muscletech.utils.XToastUtils;
import com.xuexiang.xaop.annotation.SingleClick;
import com.xuexiang.xpage.annotation.Page;
import com.xuexiang.xrouter.annotation.AutoWired;
import com.xuexiang.xrouter.launcher.XRouter;
import com.xuexiang.xui.widget.actionbar.TitleBar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

@Page(name = "Payment Details", params = {PaymentDetailsFragment.KEY_PRICE})
public class PaymentDetailsFragment extends BaseFragment<FragmentPaymentDetailsBinding> implements View.OnClickListener {
    private TextView priceDisplay;

    public final static String KEY_PRICE = "price";
    @AutoWired(name = KEY_PRICE)
    String price;

    @NonNull
    @Override
    protected FragmentPaymentDetailsBinding viewBindingInflate(LayoutInflater inflater, ViewGroup container) {
        return FragmentPaymentDetailsBinding.inflate(inflater, container, false);
    }
    /**
     * 初始化控件
     */
    @Override
    protected void initViews() {
        priceDisplay = findViewById(R.id.price);
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("venueInfo", 0);
        priceDisplay.setText(String.valueOf(sharedPreferences.getInt("price", 0)));

    }

    @Override
    protected void initListeners() {
        binding.paymentConfirmation.setOnClickListener(this);
    }

    @SingleClick
    @Override
    public void onClick(View v) {
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("venueInfo", 0);
        int id = v.getId();
        if (id == R.id.payment_confirmation) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    String venueId = String.valueOf(sharedPreferences.getInt("venueId", 1));
                    String orderDate = sharedPreferences.getString("date", null);
                    String orderTime = String.valueOf(sharedPreferences.getInt("orderTime", 1));
                    String json = "{\n" +
                            "\"venueId\":" + "\"" + venueId + "\"" +
                            ",\n\"orderTime\":" + "\"" + orderTime + "\"" +
                            ",\n\"orderDate\":" + "\"" + orderDate + "\"" +
                            "}";

                    SharedPreferences sp = getActivity().getSharedPreferences("loginToken", 0);
                    String token = sp.getString("token", null);
                    OkHttpClient client = new OkHttpClient();
                    String responseData = null;
                    Request request1 = new Request.Builder()
                            .url("http://525687l8u9.zicp.vip/card")
                            .header("token", token)
                            .build();

                    Request request2 = new Request.Builder()
                            .url("http://525687l8u9.zicp.vip/order")
                            .post(RequestBody.create(MediaType.parse("application/json"), json))
                            .header("token", token)
                            .build();

                    Response response = null;
                    try {
                        response = client.newCall(request1).execute();
                        responseData = response.body().string();
                        try{
                            JSONObject jsonObject = new JSONObject(responseData);
                            String data = jsonObject.getString("data");
                            JSONArray jsonArray = new JSONArray(data);
                            if(jsonArray.length() == 0){
                                if (getActivity() == null){
                                    return;
                                }
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        openPage(CardAdditionFragment.class, false);
                                        XToastUtils.toast("Please add card to pay");
                                    }
                                });
                            }else{
                                response = client.newCall(request2).execute();
                                responseData = response.body().string();
                                System.out.println("response data");
                                System.out.println(responseData);
                                jsonObject = new JSONObject(responseData);
                                data = jsonObject.getString("code");
                                if (data.equals("0")){
                                    popToBack();
                                    if (getActivity() == null){
                                        return;
                                    }
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            XToastUtils.success("Payment successful!");
                                        }
                                    });
                                }
                            }
                        }catch (JSONException e){
                            e.printStackTrace();
                        }


                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }
    }
    @Override
    protected void initArgs() {
        XRouter.getInstance().inject(this);
    }
}
