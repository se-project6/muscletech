/*
 * Copyright (C) 2021 xuexiangjys(xuexiangjys@163.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.xuexiang.muscletech.fragment.other;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.xuexiang.muscletech.R;
import com.xuexiang.muscletech.activity.MainActivity;
import com.xuexiang.muscletech.core.BaseFragment;
import com.xuexiang.muscletech.databinding.FragmentAccountBinding;
import com.xuexiang.muscletech.utils.TokenUtils;
import com.xuexiang.muscletech.utils.XToastUtils;
import com.xuexiang.xaop.annotation.SingleClick;
import com.xuexiang.xpage.annotation.Page;
import com.xuexiang.xui.widget.actionbar.TitleBar;
import com.xuexiang.xui.widget.dialog.DialogLoader;
import com.xuexiang.xui.widget.textview.supertextview.SuperTextView;
import com.xuexiang.xutil.XUtil;


@Page(name = "Account")
public class AccountFragment extends BaseFragment<FragmentAccountBinding> implements SuperTextView.OnSuperTextViewClickListener {

    @NonNull
    @Override
    protected FragmentAccountBinding viewBindingInflate(LayoutInflater inflater, ViewGroup container) {
        return FragmentAccountBinding.inflate(inflater, container, false);
    }


    @Override
    protected void initViews() {
        binding.menuPasswordSetting.setOnSuperTextViewClickListener(this);
        binding.menuLogout.setOnSuperTextViewClickListener(this);
    }

    @SingleClick
    @Override
    public void onClick(SuperTextView superTextView) {
        int id = superTextView.getId();
        if (id  == R.id.menu_password_setting) {
            openNewPage(PasswordSettingFragment.class);
        } else if (id == R.id.menu_logout) {
            DialogLoader.getInstance().showConfirmDialog(
                    getContext(),
                    getString(R.string.lab_logout_confirm),
                    getString(R.string.lab_yes),
                    (dialog, which) -> {
                        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("loginToken", 0);
                        String token = sharedPreferences.getString("token", null);
                        System.out.println("before clear");
                        System.out.println(token);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.clear();
                        editor.commit();

                        token = sharedPreferences.getString("token", null);
                        System.out.println("after clear");
                        System.out.println(token);
                        dialog.dismiss();
                        XUtil.getActivityLifecycleHelper().exit();
                        TokenUtils.handleLogoutSuccess();
                    },
                    getString(R.string.lab_no),
                    (dialog, which) -> dialog.dismiss()
            );
        }
    }

}
