/*
 * Copyright (C) 2019 xuexiangjys(xuexiangjys@163.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.xuexiang.muscletech.fragment.payment;

import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.xuexiang.muscletech.R;
import com.xuexiang.muscletech.core.BaseFragment;
import com.xuexiang.muscletech.databinding.FragmentCardAdditionBinding;
import com.xuexiang.muscletech.utils.XToastUtils;
import com.xuexiang.xaop.annotation.SingleClick;
import com.xuexiang.xpage.annotation.Page;
import com.xuexiang.xui.widget.actionbar.TitleBar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


@Page(name = "Card Addition")
public class CardAdditionFragment extends BaseFragment<FragmentCardAdditionBinding> implements View.OnClickListener {

    @NonNull
    @Override
    protected FragmentCardAdditionBinding viewBindingInflate(LayoutInflater inflater, ViewGroup container) {
        return FragmentCardAdditionBinding.inflate(inflater, container, false);
    }

//    @Override
//    protected TitleBar initTitle() {
//        return null;
//    }


    /**
     * 初始化控件
     */
    @Override
    protected void initViews() {

    }

    @Override
    protected void initListeners() {
        binding.addConfirm.setOnClickListener(this);
    }

    @SingleClick
    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.add_confirm) {

            String holderName = binding.addTaskTitle.getText().toString();
            String cardNumber = binding.addTaskDescription.getText().toString();
            String phoneNumber = binding.taskDate.getText().toString();

            new Thread(new Runnable() {
                @Override
                public void run() {
                    String json = "{\n" +
                            "\"cardName\":" + "\"" + holderName + "\"" +
                            ",\n\"cardNumber\":" + "\"" + cardNumber + "\"" +
                            ",\n\"cardPhone\":" + "\"" + phoneNumber + "\"" +
                            "}";
                    SharedPreferences sp = getActivity().getSharedPreferences("loginToken", 0);
                    String token = sp.getString("token", null);

                    System.out.println(json);
                    OkHttpClient client = new OkHttpClient();
                    Request request = new Request.Builder()
                            .url("http://525687l8u9.zicp.vip/card")
                            .header("token", token)
                            .post(RequestBody.create(MediaType.parse("application/json"), json))
                            .build();

                    Response response = null;
                    try {
                        response = client.newCall(request).execute();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    String responseData = null;
                    try {
                        responseData = response.body().string();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    System.out.println("responseData");
                    System.out.println(responseData);

                    try{
                        JSONObject jsonObject = new JSONObject(responseData);
                        String code = jsonObject.getString("code");
                        if (code.equals("0")){
                            request = new Request.Builder()
                                    .url("http://525687l8u9.zicp.vip/card")
                                    .header("token", token)
                                    .build();
                            try{
                                response = client.newCall(request).execute();
                                responseData = response.body().string();
                                jsonObject = new JSONObject(responseData);
                                String data = jsonObject.getString("data");
                                JSONArray cardArray = new JSONArray(data);
                                JSONObject card = cardArray.getJSONObject(0);
                                int cardId = card.getInt("cardId");

                                sp = getActivity().getSharedPreferences("cardInfo", 0);
                                SharedPreferences.Editor editor = sp.edit();
                                editor.putInt("cardId", cardId);
                                editor.commit();

                                openPage(PaymentFragment.class, false);
                                if(getActivity() == null){
                                    return;
                                }
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        XToastUtils.success("Card addition successful!");
                                    }
                                });
                            }catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }catch (JSONException e){
                        e.printStackTrace();
                    }
                }
            }).start();
        }
        else if (id == R.id.btn_card_get_verify_code) {
            XToastUtils.warning("Only Test!");
        }
    }
}
